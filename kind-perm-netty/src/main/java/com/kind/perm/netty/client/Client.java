package com.kind.perm.netty.client;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import com.kind.perm.netty.proto.Request;

/**
 * 
 * User: 李明
 * Date: 2016/1/25
 * Time: 21:45
 * To change this template use File | Settings | File Templates.
 */
public interface Client {

    public void send(Request request);

    public boolean reconnect() throws IOException, InterruptedException, ExecutionException, TimeoutException;

    public boolean isConnected();

    public void close();


}
