package com.kind.perm.netty.codec;

import java.io.IOException;

import com.kind.perm.netty.proto.Response;
import com.kind.perm.netty.utils.pack.Marshallable;
import com.kind.perm.netty.utils.pack.Unpack;

/**
 * Response消息解码类
 * User: 李明
 * Date: 2015/12/10
 * Time: 17:34
 * To change this template use File | Settings | File Templates.
 */
public class ResponseDecoder extends AbstractDecoder {


    @Override
    public Marshallable decode(Unpack unpack) throws IOException {
        Response response = new Response();
        response.unmarshal(unpack);
        return response;
    }
}
