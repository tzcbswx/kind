package com.kind.perm.netty.codec;

import java.io.IOException;

import com.kind.perm.netty.proto.IDMessage;
import com.kind.perm.netty.utils.pack.Marshallable;
import com.kind.perm.netty.utils.pack.Unpack;

/**
 * 
 * User: 李明
 * Date: 2016/3/9
 * Time: 14:47
 * To change this template use File | Settings | File Templates.
 */
public class IDMessageDecoder extends AbstractDecoder {


    @Override
    public Marshallable decode(Unpack unpack) throws IOException {
        IDMessage message = new IDMessage();
        message.unmarshal(unpack);
        return message;
    }
}
