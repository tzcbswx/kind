package com.kind.perm.core.account.service;

import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.PageView;
import com.kind.perm.core.account.domain.AccountDO;

import java.util.List;

/**
 * 用户业务处理接口<br/>
 *
 * @Date: 2016年12月12日
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
public interface AccountService {

    /**
     * 分页查询
     * @param pageQuery
     * @return
     */
	PageView<AccountDO> selectPageList(PageQuery pageQuery);



    /**
     * 保存数据
     * @param entity
     */
	int save(AccountDO entity);

    /**
     * 获取数据对象
     * @param id
     * @return
     */
    AccountDO getById(Long id);

    /**
     * 删除数据
     * @param id
     */
	void remove(Long id);
	
	/**
	 * 插入或更新对象
	 * 在微信网页授权获取微信用户之后自动完善系统用户表
	 * @param entity
	 */
	AccountDO saveOrUpdateByOpenId(AccountDO entity);
	
    /**
     * 根据条件查询列表<br/>
     *
     * @param entity
     * @return
     */
    List<AccountDO> queryList(AccountDO entity);
}
