package com.kind.perm.core.system.dao.impl;


import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.kind.common.datasource.KindDataSourceHolder;
import com.kind.common.datasource.DataSourceKey;
import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.mybatis.BaseDaoMyBatisImpl;
import com.kind.perm.core.system.dao.DictDao;
import com.kind.perm.core.system.domain.DictDO;

/**
 * 
 * 字典信息数据访问实现类. <br/>
 * 
 * @date:2017-02-27 20:28:11 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Repository
public class DictDaoImpl extends BaseDaoMyBatisImpl<DictDO, Serializable> implements DictDao {

	@Override
	public List<DictDO> page(PageQuery pageQuery) {
		return super.query(NAMESPACE + "page", pageQuery);
	}

	@Override
	public int count(PageQuery pageQuery) {
		return super.count(NAMESPACE + "count", pageQuery);
	}

	@Override
	public int saveOrUpdate(DictDO entity) {
		//KindDataSourceHolder.setDataSourceKey(DataSourceKey.YM_ORDER_DATA_SOURCE);
		if (entity.getId() == null) {
			return super.insert(NAMESPACE + "insert", entity);
		} else {
			return super.update(NAMESPACE + "update", entity);
		}
	}

	@Override
	public void remove(Long id) {
		delete(NAMESPACE + "delete", id);
	}

	@Override
	public DictDO getById(Long id) {
		return super.getById(NAMESPACE + "getById", id);
	}

	@Override
	public List<DictDO> queryList(DictDO entity) {
		return super.query(NAMESPACE + "queryList", entity);
	}

}
