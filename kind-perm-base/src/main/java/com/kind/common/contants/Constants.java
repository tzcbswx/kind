package com.kind.common.contants;

/**
 * 系统常用的常量定义. <br/>
 *
 * Date: 2016年12月6日 <br/>
 * @author 李明
 * @version
 * @since JDK 1.7
 * @see
 */
public class Constants {

    /** 成功 */
    public final static String SUCCESS = "success";


    /** Controller的动作标识key */
    public final static String CONTROLLER_ACTION = "action";
    /** Controller的动作标识value：添加 */
    public final static String CONTROLLER_ACTION_ADD = "add";
    /** Controller的动作标识value：更新 */
    public final static String CONTROLLER_ACTION_UPDATE = "update";
    
    /**
     * 文件类型 这个必须要有
     */
    public final static String COFILE_FILE = "FileDO";
    
    /**
     * 文件上传关联的名称 star
     */

     /**
      * 例子
      */
     public final static String COFILE_COMMUNITY = "CommunityDO";
     public final static String COFILE_Category = "CategoryDO";
     
     public final static String COFILE_DICT = "DictDO";     
     
     public final static String COFILE_ORGANIZATION = "OrganizationDO";
    /**
     * 文件上传关联的名称 end
     */
	
}
